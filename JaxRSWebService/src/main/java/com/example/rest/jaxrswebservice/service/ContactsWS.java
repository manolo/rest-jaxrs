package com.example.rest.jaxrswebservice.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.example.rest.database.dao.ContactDAO;
import com.example.rest.database.entity.Contact;
import com.example.rest.jaxrswebservice.commons.Permissions;
import com.example.rest.jaxrswebservice.commons.RequiresPermissions;
import com.example.rest.jaxrswebservice.commons.RequiresTransaction;
import com.example.rest.jaxrswebservice.commons.Wrapper;
import com.example.rest.jaxrswebservice.interceptor.Intercepted;

@Path("/contacts")
@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ContactsWS {
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(ContactsWS.class);
	private static final ContactDAO ContactDAO = new ContactDAO();

	@GET
	@RequiresTransaction
	@RequiresPermissions(Permissions.SELECT_CONTACT)
	@Intercepted
	public Wrapper<List<Contact>> getAll(@QueryParam(value = "limit") @DefaultValue("50") Integer limit,
	        @QueryParam(value = "offset") @DefaultValue("0") Integer offset,
	        @QueryParam(value = "order") @DefaultValue("asc") String order,
	        @QueryParam(value = "sort") @DefaultValue("id") String sort) {
		log.info("entry: getAll() : limit = {}, offset = {}, sort = {}, order = {}",
		        new Object[] { limit, offset, sort, order });

		// DB: count all & load filtered
		final Long total = ContactDAO.count();
		final List<Contact> list = ContactDAO.find(limit, offset, order, sort);

		log.info("total size of all Contacts = {}, number of loaded Contacts = {}", total, list.size());

		// SEND: response wrapper
		final Wrapper<List<Contact>> returnMe = new Wrapper<List<Contact>>();
		returnMe.setTotal(total);
		returnMe.setData(list);

		log.info("exit: getAll()");
		return returnMe;
	}

	@POST
	@RequiresTransaction
	@RequiresPermissions(Permissions.CREATE_CONTACT)
	@Intercepted
	public Wrapper<Contact> create(Contact Contact) throws NoSuchAlgorithmException {
		log.info("entry: create(), Contact = {}", Contact.toString());

		// DB: save admin
		ContactDAO.save(Contact);
		log.info("created Contact with id = {}", Contact.getId());
		log.info("exit: create()");
		return new Wrapper<Contact>(null);
	}
}
