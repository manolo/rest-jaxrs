Rest-JaxRS-Example
==================

- JaxRS Jersey RI
- Hibernate
- H2 Database
- CDI Weld RI Interceptors 
- ExtJS 4.2 
- Maven

This is my clon of https://github.com/dopoljak/Rest-JaxRS-Example.git with some modifications

You need at least tomcat-8 to run the .war.

### Building the war

  - when in the parent folder run `mvn clean package`
  - Then deploy the `JaxRSWebService/target/JaxRSWebService-1.0-SNAPSHOT.war` to the
    `webapp` folder of your Tomcat server
  - You can visit the URL `http://your_tomcat_server:8080/JaxRSWebService-1.0-SNAPSHOT`


### Running the project

  - alternativelly you can run your project directly by executing
    `mvn clean install` in the parent folder, and then in the `JaxRSWebService` running
    `mvn cargo:run`

  - Then visit the URL `http://127.0.0.1:8080/JaxRSWebService`

 

