package com.example.rest.database.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.example.rest.database.base.AuditField;
import com.example.rest.database.base.IEntity;

/**
 * 
 * @author DoDo <dopoljak@gmail.com>
 */
@Entity
@Table(name = "CONTACT")
public class Contact implements java.io.Serializable, IEntity {
	private Long id;
	private String name;
	private String last;
	private String image;

	public Contact() {
	}

	@Id
	@GeneratedValue
	@Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME", length = 160)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "LAST", length = 160)
	public String getlast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	@Column(name = "IMAGE", length = 400)
	public String getEmail() {
		return image;
	}

	public void setEmail(String image) {
		this.image = image;
	}
	
    @Override
    @Transient
    public AuditField[] getAuditFields()
    {
        AuditField auditFields[] = 
        {
            new AuditField(1, id),
            new AuditField(2, name),
            new AuditField(3, last),
            new AuditField(3, image)
        };

        return auditFields;
    }

    
    @Override
    public void validateMandatoryFields() throws IllegalArgumentException
    {
        if(name == null || name.isEmpty() )
        {
            throw new IllegalArgumentException("Name field is NULL or Empty");
        }
    }

}
