package com.example.rest.database.dao;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.example.rest.database.base.GenericDAO;
import com.example.rest.database.entity.Contact;
import com.example.rest.database.entity.User;
import com.example.rest.database.entity.UserStatus;

public class ContactDAO  extends GenericDAO<Contact, Serializable>
{
    public ContactDAO()
    {
	super(Contact.class);
    }

}